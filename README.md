# IaC Group 6 - Load balanced Gitlab instance

This was a university assignment at [NTNU](https://www.ntnu.edu/) for the Course [Infrastructure as Code](https://www.ntnu.edu/studies/courses/IIKG3005/2021/1).

This repository has the configuration to deploy a multi node Gitlab installation, based on the [2k users Gitlab Reference Architecture](https://docs.gitlab.com/ee/administration/reference_architectures/2k_users.html), on Openstack.

The Project was created by Nadine Leuthe and Jonas Holik.

## Content

`movie` here you find the video we had to create for the assignment to display our work. 

`terraform/gitlab` in this folder are the terraform files for setting up the vms and the network for Gitlab.

`terraform/puppet` in this folder are the terraform files for setting up the Puppet Server.

`puppet` in this folder are the puppet files to configure the Gitlab servers.

## Setup

To setup the gitlab instance, you need to be authenticated on the Openstack cli.

Then go to the `terraform/puppet` folder and update the image and ssh-key in the `compute.tf` file.
Afterwards initialize terraform (`terraform init`) and apply (`terraform apply`) the configuration.

When terraform is finished, copy the content of the `puppet` folder to the created puppetmasters production environment:
`scp -r puppet/* <your-user>@<puppetmasterip>:/etc/puppetlabs/code/environments/production/`

Now go to the `terraform/gitlab` folder and update the `terraform.tfvars` with your ssh-key, the baseimage and the ip of the puppet master. 
Then initialize terraform (`terraform init`) and apply (`terraform apply`) the configuration.

Puppet will now take over and configure all the servers.
After waiting for about 20-30 minutes Gitlab should be available under the outputted rails ips.

## Flaws

Those scripts have some flaws which are there because of restricted time for the project.

- Because of the time constraints, we weren't able to to configure the HAProxy. So there is no load balancing between the two web interfaces.
- The Puppet master autosigns every client that connects to it.
- We use static gitlab-secrets which are also checked into this repository!
- Credentials are hard-coded into puppet configuration files

These are probably the worst problems in this configuration.