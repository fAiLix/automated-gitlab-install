class profile::gitlabinstall::prometheus {
  class { 'gitlab':
    roles        => ['monitoring_role'],
    external_url => 'http://gitlab.novalocal',
    prometheus   => {
      listen_address     => '0.0.0.0:9090',
      monitor_kubernetes => false,
      scrape_configs     => [
        {
          job_name       => 'postgres',
          static_configs => {
            targets => ['192.168.100.61:9187'],
          },
        },
        {
          job_name       => 'redis',
          static_configs => {
            targets => ['192.168.100.62:9121'],
          },
        },
        {
          job_name       => 'gitaly',
          static_configs => {
            targets => ['192.168.100.63:9236'],
          },
        },
        {
          job_name       => 'gitlab-nginx',
          static_configs => {
            targets => ['192.168.100.70:8060', '192.168.100.71:8060'],
          },
        },
        {
          job_name       => 'gitlab-workhorse',
          static_configs => {
            targets => ['192.168.100.70:9229', '192.168.100.71:9229'],
          },
        },
        {
          job_name       => 'gitlab-rails',
          static_configs => {
            targets => ['192.168.100.70:8080', '192.168.100.71:8080'],
          },
        },
        {
          job_name       => 'gitlab-sidekiq',
          static_configs => {
            targets => ['192.168.100.70:8082', '192.168.100.71:8082'],
          },
        },
        {
          job_name       => 'node',
          static_configs => {
            targets => ['192.168.100.61:9100', '192.168.100.62:9100', '192.168.100.63:9100', '192.168.100.70:9100', '192.168.100.71:9100'],
          },
        },
      ],
    },
    grafana      => {
      enable             => true,
      admin_password     => 'iacgroup6',
      disable_login_form => false,
    },

    nginx        => {
      enable => true,
    },
  }
  file { 'gitlab-secrets.json':
    ensure             => 'file',
    path               => '/etc/gitlab/gitlab-secrets.json',
    group              => 'root', # Which group should own the file.  Argument can...
    mode               => '0600', # The desired permissions mode for the file, in...
    owner              => 'root', # The user to whom the file should belong....
    replace            => true, # Whether to replace a file or symlink that...
    source             => 'puppet:///modules/profile/gitlab-secrets.json',# A source file, which will be copied into place...
    source_permissions => 'ignore', # Whether (and how) Puppet should copy owner...
  }
}
