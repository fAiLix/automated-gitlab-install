class profile::gitlabinstall::gitaly {
  class { 'gitlab':
    gitaly           => {
      auth_token             => 'iacgroup6',
      enable                 => true,
      listen_addr            => '0.0.0.0:8075',
      prometheus_listen_addr => '0.0.0.0:9236',
    },
    shell            => {
      secret_token => 'shellsecret',
    },
    postgresql       => {
      enable => false,
    },
    redis            => {
      enable => false,
    },
    puma             => {
      enable => false,
    },
    sidekiq          => {
      enable => false,
    },
    gitlab_workhorse => {
      enable => false,
    },
    prometheus       => {
      enable => false,
    },
    alertmanager     => {
      enable => false,
    },
    grafana          => {
      enable => false,
    },
    gitlab_exporter  => {
      enable => false,
    },
    nginx            => {
      enable => false,
    },

    gitlab_rails     => {
      auto_migrate     => false,
      internal_api_url => 'http://192.168.100.70/', # GitLab URL or an internal load
    },
    node_exporter    => {
      listen_address =>'0.0.0.0:9100',
    },
    git_data_dirs    => {
      'default'  => {
        'path' => '/var/opt/gitlab/git-data'
      },
      'storage1' => {
        'path' => '/mnt/gitlab/git-data'
      }
    },
  }
  file { 'gitlab-secrets.json':
    ensure             => 'file',
    path               => '/etc/gitlab/gitlab-secrets.json',
    group              => 'root', # Which group should own the file.  Argument can...
    mode               => '0600', # The desired permissions mode for the file, in...
    owner              => 'root', # The user to whom the file should belong....
    replace            => true, # Whether to replace a file or symlink that...
    source             => 'puppet:///modules/profile/gitlab-secrets.json',# A source file, which will be copied into place...
    source_permissions => 'ignore', # Whether (and how) Puppet should copy owner...
  }
}
