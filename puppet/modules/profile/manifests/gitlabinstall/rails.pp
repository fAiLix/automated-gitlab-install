# @summary Installs the rails service for a 2k user gitlab reference architecture
#
# Installs the gitlab rails service to host the frontend
#
# @example
#   include gitlab::gitlabrails
class profile::gitlabinstall::rails {
  class { 'gitlab':
    external_url     => 'http://gitlab.novalocal',
    gitlab_rails     => {
      'initial_root_password'    => 'iacgroup6',
      'gitaly_token'             => 'iacgroup6',
      'db_adapter'               => 'postgresql',
      'db_encoding'              => 'unicode',
      'db_host'                  => '192.168.100.61',
      'db_password'              => 'iacgroup6',
      'redis_port'               => '6379',
      'redis_host'               => '192.168.100.62',
      'redis_password'           => 'iacgroup6',
      'monitoring_whitelist'     => ['192.168.100.66/32', '127.0.0.0/8'],
      'artifacts_enabled'        => false,
      'terraform_state_enabled'  => false,
      'external_diffs_enabled'   => false,
      'dependency_proxy_enabled' => false,
      'lfs_enabled'              => false,
      'packages_enabled'         => false,
    },
    shell            => {
      'secret_token' => 'iacgroup6'
    },
    git_data_dirs    => {
      'default'  => { 'gitaly_address' => 'tcp://192.168.100.63:8075' },
      'storage1' => { 'gitaly_address' => 'tcp://192.168.100.63:8075' },
    },
    roles            => ['application_role'],
    gitaly           => {
      'enable' => false,
    },
    nginx            => {
      'enable' => true,
      'status' => {
        'options' => {
          'allow' => ['192.168.100.66/32', '127.0.0.0/8'],
        },
      },
    },
    node_exporter    => {
      'listen_address' => '0.0.0.0:9100',
    },
    gitlab_workhorse => {
      'prometheus_listen_addr' => '0.0.0.0:9229',
    },
    sidekiq          => {
      'listen_address'  => '0.0.0.0',
      'max_concurrency' => 10,
    },
    puma             => {
      'listen' => '0.0.0.0',
    },
  }
  file { 'gitlab-secrets.json':
    ensure             => 'file',
    path               => '/etc/gitlab/gitlab-secrets.json',
    group              => 'root', # Which group should own the file.  Argument can...
    mode               => '0600', # The desired permissions mode for the file, in...
    owner              => 'root', # The user to whom the file should belong....
    replace            => true, # Whether to replace a file or symlink that...
    source             => 'puppet:///modules/profile/gitlab-secrets.json',# A source file, which will be copied into place...
    source_permissions => 'ignore', # Whether (and how) Puppet should copy owner...
  }
}
