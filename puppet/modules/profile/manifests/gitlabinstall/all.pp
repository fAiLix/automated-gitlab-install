class profile::gitlabinstall::all {
  class { 'gitlab':
    external_url => "http://${facts['fqdn']}"
  }
}
