# @summary Installs the rails service for a 2k user gitlab reference architecture
#
# Installs the gitlab rails service to host the frontend
#
# @example
#   include gitlab::gitlabrails
class profile::gitlabinstall::pgsql {
  class { 'gitlab':
    roles              => ['postgres_role'],
    prometheus         => {
      'enable' => false,
    },
    alertmanager       => {
      'enable' => false,
    },
    pgbouncer_exporter => {
      'enable' => false,
    },
    redis_exporter     => {
      'enable' => false,
    },
    gitlab_exporter    => {
      'enable' => false,
    },
    node_exporter      => {
      'listen_address' => '0.0.0.0:9100',
    },
    postgres_exporter  => {
      'listen_address' => '0.0.0.0:9187',
      'dbname'         => 'gitlabhq_production',
      'password'       => '11b4148b9410b022ba86d8f6e29547c2',
    },
    postgresql         => {
      'listen_address'            => '0.0.0.0',
      'port'                      => 5432,
      'sql_user_password'         => '5aa575144f474b0b73ba3c0e5d2f7e62',
      'trust_auth_cidr_addresses' => ['127.0.0.1/32', '192.168.100.1/24'],
    },
    gitlab_rails       => {
      'auto_migrate' => false
    },
  }
  file { 'gitlab-secrets.json':
    ensure             => 'file',
    path               => '/etc/gitlab/gitlab-secrets.json',
    group              => 'root',
    mode               => '0600',
    owner              => 'root',
    replace            => true,
    source             => 'puppet:///modules/profile/gitlab-secrets.json',
    source_permissions => 'ignore',
  }
}
