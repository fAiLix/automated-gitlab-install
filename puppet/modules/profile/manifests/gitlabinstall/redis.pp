class profile::gitlabinstall::redis {
  class { 'gitlab':
    redis            => {
      'enable'   => true,
      'bind'     => '0.0.0.0',
      'port'     => 6379,
      'password' => 'iacgroup6',
    },
    gitaly           => {
      'enable' => false,
    },
    postgresql       => {
      'enable' => false,
    },
    puma             => {
      'enable' => false,
    },
    sidekiq          => {
      'enable' => false,
    },
    gitlab_workhorse => {
      'enable' => false,
    },
    prometheus       => {
      'enable' => false,
    },
    alertmanager     => {
      'enable' => false,
    },
    grafana          => {
      'enable' => false,
    },
    gitlab_exporter  => {
      'enable' => false,
    },
    nginx            => {
      'enable' => false,
    },
    gitlab_rails     => {
      'enable' => false,
    },
    node_exporter    => {
      'listen_address' => '0.0.0.0:9100',
    },
    redis_exporter   => {
      'listen_address' => '0.0.0.0:9121',
      'flags'          => {
        'redis.addr'     => 'redis://0.0.0.0:6379',
        'redis.password' => 'iacgroup6'
      }
    },
  }
  file { 'gitlab-secrets.json':
    ensure             => 'file',
    path               => '/etc/gitlab/gitlab-secrets.json',
    group              => 'root', # Which group should own the file.  Argument can...
    mode               => '0600', # The desired permissions mode for the file, in...
    owner              => 'root', # The user to whom the file should belong....
    replace            => true, # Whether to replace a file or symlink that...
    source             => 'puppet:///modules/profile/gitlab-secrets.json',# A source file, which will be copied into place...
    source_permissions => 'ignore', # Whether (and how) Puppet should copy owner...
  }
}
