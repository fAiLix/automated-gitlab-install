node 'default' {
  # include role::gitlab_all
}

node 'gitlab-psql.novalocal' {
  include role::gitlab_pgsql
}

node 'gitlab-gitaly.novalocal' {
  include role::gitlab_gitaly
}

node 'gitlab-redis.novalocal' {
  include role::gitlab_redis
}

node /^gitlab-rails-\d\.novalocal$/ {
  include role::gitlab_rails
}

node 'gitlab-monitoring.novalocal' {
  include role::gitlab_prometheus
}
