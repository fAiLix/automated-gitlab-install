resource "openstack_networking_network_v2" "management_net" {
  name           = "management_net"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "management_subnet" {
  name       = "management_subnet"
  network_id = "${openstack_networking_network_v2.management_net.id}"
  cidr       = "192.168.110.0/24"
  ip_version = 4
}

resource "openstack_networking_router_v2" "management_router" {
  name                = "management_router"
  admin_state_up      = true
  external_network_id = "730cb16e-a460-4a87-8c73-50a2cb2293f9" # ntnu-internal
}

resource "openstack_networking_router_interface_v2" "management_router_interface_management_subnet" {
  router_id = "${openstack_networking_router_v2.management_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.management_subnet.id}"
}

resource "openstack_networking_secgroup_v2" "puppet" {
  name        = "puppet"
  description = "Puppet security group"
}

resource "openstack_networking_secgroup_rule_v2" "puppet_ping" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.puppet.id}"
}

resource "openstack_networking_secgroup_rule_v2" "puppet_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.puppet.id}"
}

resource "openstack_networking_secgroup_rule_v2" "puppet_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.puppet.id}"
}

resource "openstack_networking_secgroup_rule_v2" "puppet_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.puppet.id}"
}

resource "openstack_networking_secgroup_rule_v2" "puppet_agent" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8140
  port_range_max    = 8140
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.puppet.id}"
}

resource "openstack_networking_floatingip_v2" "floatip_puppet" {
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_puppet" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_puppet.address}"
  instance_id = "${openstack_compute_instance_v2.puppet_master.id}"
}


output "puppet_ip" {
  value = "${openstack_networking_floatingip_v2.floatip_puppet.address}"
}

output "puppet_local_ips" {
  value = "${openstack_compute_instance_v2.puppet_master.network.*.fixed_ip_v4}"
}

resource "openstack_networking_port_v2" "static_ip" {
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.management_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.puppet.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.management_subnet.id}"
    ip_address   = "192.168.110.90"

  }
}