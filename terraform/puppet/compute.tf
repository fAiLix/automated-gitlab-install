data "template_file" "user_data" {
  template = file("cloud_init.yml")
}

resource "openstack_compute_instance_v2" "puppet_master" {
  name            = "Puppet"
  image_name        = "CentOS 8 Stream x86_64"
  flavor_name     = "m1.small"
  key_pair        = "nadinecl"
  security_groups = ["${openstack_networking_secgroup_v2.puppet.id}"]

  network {
    port = "${openstack_networking_port_v2.static_ip.id}"
  }

  user_data = data.template_file.user_data.rendered

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.management_subnet] 
}

