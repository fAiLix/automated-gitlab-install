variable "puppetserver" {
  type    = string
  default = ""
}

variable "baseimage" {
  type    = string
  default = "CentOS 8 Stream x86_64"
}

variable "sshkey" {
  type    = string
  default = ""
}