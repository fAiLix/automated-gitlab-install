resource "openstack_networking_network_v2" "gitlab_net" {
  name           = "gitlab_net"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "gitlab_subnet" {
  name       = "gitlab_subnet"
  network_id = "${openstack_networking_network_v2.gitlab_net.id}"
  cidr       = "192.168.100.0/24"
  ip_version = 4
}

resource "openstack_networking_router_v2" "gitlab_router" {
  name                = "gitlab_router"
  admin_state_up      = true
  external_network_id = "730cb16e-a460-4a87-8c73-50a2cb2293f9" # ntnu-internal
}

resource "openstack_networking_router_interface_v2" "gitlab_router_interface_gitlab_subnet" {
  router_id = "${openstack_networking_router_v2.gitlab_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
}

resource "openstack_networking_secgroup_v2" "gitlab" {
  name        = "gitlab"
  description = "Default security group"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_ping" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "icmp"
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_http" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_https" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_ssh" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_pgsql" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 5432
  port_range_max    = 5432
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_redis" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 6379
  port_range_max    = 6379
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_puma" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8080
  port_range_max    = 8080
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_workhorse" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8181
  port_range_max    = 8181
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_nginx_status" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8060
  port_range_max    = 8060
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_prometheus" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9090
  port_range_max    = 9090
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_node_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9100
  port_range_max    = 9100
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_redis_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9121
  port_range_max    = 9121
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_psql_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9187
  port_range_max    = 9187
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9168
  port_range_max    = 9168
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_sidekiq_exporter" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8082
  port_range_max    = 8082
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_secgroup_rule_v2" "gitlab_gitaly" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 8075
  port_range_max    = 8075
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.gitlab.id}"
}

resource "openstack_networking_floatingip_v2" "floatip_loadbalancer" {
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_loadbalancer" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_loadbalancer.address}"
  instance_id = "${openstack_compute_instance_v2.gitlab_loadbalancer.id}"
}

output "load_balancer_ip" {
  value = "${openstack_networking_floatingip_v2.floatip_loadbalancer.address}"
}

resource "openstack_networking_floatingip_v2" "floatip_rails" {
  count = 2
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_rails" {
  count = 2
  floating_ip = "${openstack_networking_floatingip_v2.floatip_rails[count.index].address}"
  instance_id = "${openstack_compute_instance_v2.gitlab_rails[count.index].id}"
}

output "rails_ip_0" {
  value = "${openstack_networking_floatingip_v2.floatip_rails[0].address}"
}
output "rails_ip_1" {
  value = "${openstack_networking_floatingip_v2.floatip_rails[1].address}"
}

resource "openstack_networking_floatingip_v2" "floatip_psql" {
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_psql" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_psql.address}"
  instance_id = "${openstack_compute_instance_v2.gitlab_psql.id}"
}

output "psql_ip" {
  value = "${openstack_networking_floatingip_v2.floatip_psql.address}"
}

resource "openstack_networking_floatingip_v2" "floatip_redis" {
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_redis" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_redis.address}"
  instance_id = "${openstack_compute_instance_v2.gitlab_redis.id}"
}

output "redis_ip" {
  value = "${openstack_networking_floatingip_v2.floatip_redis.address}"
}

resource "openstack_networking_floatingip_v2" "floatip_gitaly" {
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_gitaly" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_gitaly.address}"
  instance_id = "${openstack_compute_instance_v2.gitlab_gitaly.id}"
}

output "gitaly_ip" {
  value = "${openstack_networking_floatingip_v2.floatip_gitaly.address}"
}

resource "openstack_networking_floatingip_v2" "floatip_monitoring" {
  pool = "ntnu-internal"
}

resource "openstack_compute_floatingip_associate_v2" "floatip_monitoring" {
  floating_ip = "${openstack_networking_floatingip_v2.floatip_monitoring.address}"
  instance_id = "${openstack_compute_instance_v2.gitlab_monitoring.id}"
}

output "monitoring_ip" {
  value = "${openstack_networking_floatingip_v2.floatip_monitoring.address}"
}

resource "openstack_networking_port_v2" "static_ip_loadbalancer" {
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.gitlab_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
    ip_address   = "192.168.100.60"

  }
}

resource "openstack_networking_port_v2" "static_ip_psql" {
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.gitlab_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
    ip_address   = "192.168.100.61"

  }
}

resource "openstack_networking_port_v2" "static_ip_redis" {
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.gitlab_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
    ip_address   = "192.168.100.62"

  }
}

resource "openstack_networking_port_v2" "static_ip_gitaly" {
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.gitlab_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
    ip_address   = "192.168.100.63"

  }
}

resource "openstack_networking_port_v2" "static_ip_rails" {
  count          = 2
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.gitlab_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
    ip_address   = "192.168.100.7${count.index}"

  }
}

resource "openstack_networking_port_v2" "static_ip_monitoring" {
  name           = "static_ip"
  network_id     = "${openstack_networking_network_v2.gitlab_net.id}"
  security_group_ids = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  admin_state_up = "true"

  fixed_ip {
    subnet_id    = "${openstack_networking_subnet_v2.gitlab_subnet.id}"
    ip_address   = "192.168.100.66"

  }
}

