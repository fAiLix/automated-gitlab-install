data "template_file" "user_data" {
  template = file("cloud_init.yml")
  vars     = {
    puppetserver   = var.puppetserver
  }
}

resource "openstack_compute_instance_v2" "gitlab_loadbalancer" {
  name            = "gitlab_loadbalancer"
  image_name        = var.baseimage
  flavor_name     = "t1.xlarge"
  key_pair        = var.sshkey
  security_groups = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  user_data = data.template_file.user_data.rendered

  network {
    port = "${openstack_networking_port_v2.static_ip_loadbalancer.id}"
  }

  network {
    name = "management_net" # management-net
  }

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.gitlab_subnet] 
}

resource "openstack_compute_instance_v2" "gitlab_psql" {
  name            = "gitlab_psql"
  image_name        = var.baseimage
  flavor_name     = "m1.medium"
  key_pair        = var.sshkey
  security_groups = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  user_data = data.template_file.user_data.rendered

  network {
    port = "${openstack_networking_port_v2.static_ip_psql.id}"
  }

  network {
    name = "management_net" # management-net
  }

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.gitlab_subnet] 
}

resource "openstack_compute_instance_v2" "gitlab_redis" {
  name            = "gitlab_redis"
  image_name        = var.baseimage
  flavor_name     = "m1.tiny"
  key_pair        = var.sshkey
  security_groups = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  user_data = data.template_file.user_data.rendered

  network {
    port = "${openstack_networking_port_v2.static_ip_redis.id}"
  }

  network {
    name = "management_net" # management-net
  }

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.gitlab_subnet] 
}

resource "openstack_compute_instance_v2" "gitlab_gitaly" {
  name            = "gitlab_gitaly"
  image_name        = var.baseimage
  flavor_name     = "m1.large"
  key_pair        = var.sshkey
  security_groups = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  user_data = data.template_file.user_data.rendered

  network {
    port = "${openstack_networking_port_v2.static_ip_gitaly.id}"
  }

  network {
    name = "management_net" # management-net
  }

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.gitlab_subnet] 
}


resource "openstack_compute_instance_v2" "gitlab_rails" {
  count = 2
  name            = "gitlab_rails_${count.index}"
  image_name        = var.baseimage
  flavor_name     = "m1.medium"
  key_pair        = var.sshkey
  security_groups = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  user_data = data.template_file.user_data.rendered

  network {
    port = "${openstack_networking_port_v2.static_ip_rails[count.index].id}"
  }

  network {
    name = "management_net" # management-net
  }

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.gitlab_subnet]
}

resource "openstack_compute_instance_v2" "gitlab_monitoring" {
  name            = "gitlab_monitoring"
  image_name        = var.baseimage
  flavor_name     = "t1.xlarge"
  key_pair        = var.sshkey
  security_groups = ["${openstack_networking_secgroup_v2.gitlab.id}"]
  user_data = data.template_file.user_data.rendered

  network {
    port = "${openstack_networking_port_v2.static_ip_monitoring.id}"
  }

  network {
    name = "management_net" # management-net
  }

  # This is needed, because terraform thinks only the network is a dependency.
  # But without a subnet on the network, Openstack will throw an error on instance creation
  depends_on = [openstack_networking_subnet_v2.gitlab_subnet] 
}
